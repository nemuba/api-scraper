class GetJob < ApplicationJob
  queue_as :default

  rescue_from(ActiveRecord::RecordNotFound) do |exception|
    puts "Dados não foram encontrados."
  end

  def perform(*args)
    begin
      categories = Category.all

      categories.each do |category|
        result = ScraperService.perform(category.description)
        result.each do |job|
          Job.find_or_create_by(title: job[:title], description: job[:description], category_id: category.id)
        end
      end
    rescue StandardError => e
      puts e
    end  
  end
end
