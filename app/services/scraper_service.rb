require 'nokogiri'
require 'open-uri'

class ScraperService
  def self.perform(search)
    begin
      page_scraped = scrape_page("https://programathor.com.br/jobs-#{search}")    
      result = []

      doc = Nokogiri::HTML(page_scraped, nil, 'utf-8')
      

      doc.css('div.container div.row div.col-md-9 div.cell-list div.row').each do |job|
        title = job.css('div.col-sm-9 div.cell-list-content h3').text
        description = job.css('div.col-sm-9 div.cell-list-content div.cell-list-content-icon span').map(&:text).join(', ')        
        result << {title: title, description: description}
      end

      result
    rescue NoMethodError => e
      puts e
    rescue StandardError => e
      puts e
    end

  end

  private
  def self.scrape_page(url)
    open(url).read
  end
end